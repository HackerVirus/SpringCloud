package org.crazyit.cloud.fallback;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

public class ChainTest {

	public static void main(String[] args) {
		CommandA c = new CommandA();
		c.execute();
	}

	static class CommandA extends HystrixCommand<String> {
		
		public CommandA() {
			super(HystrixCommandGroupKey.Factory.asKey("CommandA"));
		}

		@Override
		protected String run() throws Exception {
			System.out.println("PrimaryCommand run 方法");
			throw new RuntimeException();
		}

		@Override
		protected String getFallback() {
			return new CommandB().execute();
		}	
	}
	
	static class CommandB extends HystrixCommand<String> {
		
		public CommandB() {
			super(HystrixCommandGroupKey.Factory.asKey("CommandB"));
		}

		@Override
		protected String run() throws Exception {
			System.out.println("CommandB run 方法");
			return "";
		}

	}
}
