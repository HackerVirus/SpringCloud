package org.crazyit.feign;

import feign.Feign;
import feign.Logger;

public class TestLog {

	public static void main(String[] args) {
		// 获取服务接口
		PersonClient personClient = Feign.builder()
				.logLevel(Logger.Level.HEADERS)
				.logger(new Logger.JavaLogger().appendToFile("logs/http.log"))
				.target(PersonClient.class, "http://localhost:8080/");
		personClient.sayHello();
	}

}
