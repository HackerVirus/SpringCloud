package com.netflix.hystrix.crazyit.run;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

public class ThreadCommandC extends HystrixCommand<String> {

	public ThreadCommandC() {
	    super(HystrixCommandGroupKey.Factory.asKey("ThreadCommandC"));
	}

	@Override
	protected String run() throws Exception {
		System.out.println("这是 ThreadCommandC");
		return "success";
	}

}
