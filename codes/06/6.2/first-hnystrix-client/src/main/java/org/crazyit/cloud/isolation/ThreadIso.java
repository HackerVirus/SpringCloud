package org.crazyit.cloud.isolation;

import java.util.concurrent.ThreadPoolExecutor;

import com.netflix.config.ConfigurationManager;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;

/**
 * 线程隔离策略测试类
 * @author 杨恩雄
 *
 */
public class ThreadIso {

	public static void main(String[] args) throws Exception {
		// 配置线程池大小为3
		ConfigurationManager.getConfigInstance().setProperty(
				"hystrix.threadpool.default.coreSize", 3);
		
		for(int i = 0; i < 6; i++) {
			MyCommand c = new MyCommand(i);
			c.queue();
		}
		Thread.sleep(5000);
	}
}
