package org.crazyit.feign;

import org.crazyit.feign.PersonClient.Person;

import feign.Feign;
import feign.gson.GsonEncoder;

public class MyClientTest {

	public static void main(String[] args) {
		// 获取服务接口
		PersonClient personClient = Feign.builder()
				.encoder(new GsonEncoder())
				.client(new MyFeignClient())
				.target(PersonClient.class, "http://localhost:8080/");
		// 请求Hello World接口
		String result = personClient.sayHello();
		System.out.println("    接口响应内容：" + result);
	}
}
