package org.crazyit.feign.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;

public class MyInterceptor implements RequestInterceptor {

	public void apply(RequestTemplate template) {
		template.header("Content-Type", "application/json");
	}
}
