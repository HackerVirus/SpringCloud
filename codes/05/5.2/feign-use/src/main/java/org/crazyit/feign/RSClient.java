package org.crazyit.feign;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

public interface RSClient {

	@GET @Path("/hello")
	String rsHello();
}
