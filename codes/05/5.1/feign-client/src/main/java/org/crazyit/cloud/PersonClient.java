package org.crazyit.cloud;

import lombok.Data;
import lombok.NoArgsConstructor;
import feign.Param;
import feign.RequestLine;

/**
 * Person客户端服务类
 * @author 杨恩雄
 *
 */
public interface PersonClient {

	@RequestLine("GET /person/{personId}")
	Person findById(@Param("personId") Integer personId);
	
	@Data // 为所有属性加上setter和getter等方法
	class Person {
		Integer id;
		String name;
		Integer age;
		String message;
	}
}
